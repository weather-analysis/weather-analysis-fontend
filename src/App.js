import React from 'react';
import './App.css';
import EchartsTest from "./components/TestComp"
import Nest from './components/Nest';
function App() {
  return (
    <div className="App">
      <Nest></Nest>
      <EchartsTest></EchartsTest>
    </div>
  );
}

export default App;
