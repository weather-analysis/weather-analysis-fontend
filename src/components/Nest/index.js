import React from 'react';
import ReactCanvasNest from 'react-canvas-nest';


const Nest = () => {
    return (
        <ReactCanvasNest className='canvasNest' config={{ pointR:3, pointColor: '255,255,153', count: 99,lineColor:"255,255,0" }} style={{ zIndex: -99 }} />
    )
}

export default Nest;