import React, { Component } from 'react';
import 'zrender/lib/svg/svg';
// 引入 ECharts 主模块
import echarts from 'echarts/lib/echarts';
import 'echarts/map/js/china.js';
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
class EchartsTest extends Component {
    componentDidMount() {
        // 基于准备好的dom，初始化echarts实例
        function randomValue() {
            return Math.round(Math.random()*1000);
        }
        var dataList=[
            {degree:32,name:"南海诸岛",value:0},
            {degree:32,name: '北京', value: randomValue()},
            {degree:32,name: '天津', value: randomValue()},
            {degree:32,name: '上海', value: randomValue()},
            {degree:32,name: '重庆', value: randomValue()},
            {degree:32,name: '河北', value: randomValue()},
            {degree:32,name: '河南', value: randomValue()},
            {degree:32,name: '云南', value: randomValue()},
            {degree:32,name: '辽宁', value: randomValue()},
            {degree:32,name: '黑龙江', value: randomValue()},
            {degree:32,name: '湖南', value: randomValue()},
            {degree:32,name: '安徽', value: randomValue()},
            {degree:32,name: '山东', value: randomValue()},
            {degree:32,name: '新疆', value: randomValue()},
            {degree:32,name: '江苏', value: randomValue()},
            {degree:32,name: '浙江', value: randomValue()},
            {degree:32,name: '江西', value: randomValue()},
            {degree:32,name: '湖北', value: randomValue()},
            {degree:32,name: '广西', value: randomValue()},
            {degree:32,name: '甘肃', value: randomValue()},
            {degree:32,name: '山西', value: randomValue()},
            {degree:32,name: '内蒙古', value: randomValue()},
            {degree:32,name: '陕西', value: randomValue()},
            {degree:32,name: '吉林', value: randomValue()},
            {degree:32,name: '福建', value: randomValue()},
            {degree:32,name: '贵州', value: randomValue()},
            {degree:32,name: '广东', value: randomValue()},
            {degree:32,name: '青海', value: randomValue()},
            {degree:32,name: '西藏', value: randomValue()},
            {degree:32,name: '四川', value: randomValue()},
            {degree:32,name: '宁夏', value: randomValue()},
            {degree:32,name: '海南', value: randomValue()},
            {degree:32,name: '台湾', value: randomValue()},
            {degree:32,name: '香港', value: randomValue()},
            {degree:32,name: '澳门', value: randomValue()}
        ]
        var myChart = echarts.init(document.getElementById('main'), {renderer: 'svg'});
        const option = {
            tooltip: {
                    formatter:function(params,ticket, callback){
                        return params.seriesName+'<br />'+params.name+'：'+params.value+"温度"+params.data.degree
                    }
                },
            visualMap: {
                min: 0,
                max: 1500,
                left: 'left',
                top: 'bottom',
                text: ['高','低'],
                inRange: {
                    color: ['#e0ffff', '#006edd']
                },
                show:true
            },
            geo: {
                map: 'china',
                roam: true, //是否开启鼠标缩放和平移漫游
                zoom:1.23,
                label: {
                    normal: {
                        show: true,
                        fontSize:'10',
                        color: 'rgba(0,0,0,0.7)'
                    }
                },
                itemStyle: {
                    normal:{
                        borderColor: 'rgba(0, 0, 0, 0.2)'
                    },
                    emphasis:{
                        areaColor: '#F3B329',
                        shadowOffsetX: 0,
                        shadowOffsetY: 0,
                        shadowBlur: 20,
                        borderWidth: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            },
            series : [
                {
                    name: '天气情况',
                    type: 'map',
                    geoIndex: 0,
                    data:dataList
                },
            ],
            
        };
        myChart.setOption(option);
        myChart.on('click', function (params) {
            console.log(params);
        });
        
    }
    render() {
        return (
            <div id="main" style={{ width: "100vw", height: "100vh" }}></div>
        );
    }
}

export default EchartsTest;